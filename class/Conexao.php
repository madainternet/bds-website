<?php
class Conexao {
    static private $host 	= 'localhost'; 
    static private $user 	= 'root'; //brasildasorte_user
    static private $pass 	= ''; //'Ma1nt4++'
    static private $db   	= 'brasildasorte'; //brasildasorte_site
	static private $charset = 'utf8';

    private $conexao;

    public function __construct($getLocal = false){
        try{
            $this->conexao = new PDO('mysql:host='.self::$host.';dbname='.self::$db.';charset='.self::$charset, self::$user, self::$pass);
        }catch(PDOException $e) {
            die('ERROR: ' . $e->getMessage());
        }
    }

    public function select($table, $colunas = '*', $inner = false, array $where, $extsql = '', $retorno = 'array'){
        $condicao   = '';
        $bind       = [];
        $busca      = [];
        $count_cond = 0; 

        if(count($where) > 0){
            foreach($where as $valor){
                $count_cond++;
                $op = ($count_cond === 1) ? 'WHERE ' : ' AND ';
                
                $condicao .= $op.$valor[0].' '.$valor[1].' :'.$valor[0];       
                $bind[':'.$valor[0]] = $valor[2];
            }
        }

        if(!$inner) $inner = ' ';

        try {
            $stmt = $this->conexao->prepare('SELECT '.$colunas.' FROM '.$table.$inner.$condicao.' '.$extsql);
            
            //var_dump('SELECT '.$colunas.' FROM '.$table.$inner.$condicao.' '.$extsql);
            foreach($bind as $key=>$val){
                $stmt->bindValue($key, $val, PDO::PARAM_STR);
            }

            $stmt->execute();
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
            if ( count($result) ) { 
                foreach($result as $row) {
                    $busca[] = $row;
                }   
            } else {
                $busca = false;
            }
            return ($retorno === 'array') ? $busca : json_encode($busca);

        } catch(PDOException $e) {
            die( 'ERROR: ' . $e->getMessage() );
        }
    }
}
?>