<?php
require_once 'autoload.php';

class Sorteio{
    private $acesso; // Instancia do Banxco de dados
    static private $table = 'sorteios';
    private $local;

    public function __construct($local = false){
        $this->acesso = new Conexao();
        $this->local  = $local;
    }

    public function getUltimo(){
        if(!$this->local){
            return false;
        }else{
            $sorteio = $this->acesso->select(self::$table, 
                                        '*', 
                                        false,
                                        [['status', '=', 'T'], ['local_id', '=', $this->local->id]], 
                                        'ORDER BY id DESC LIMIT 1') [0];
            return $this->obj($sorteio);                            
        }
    }

    /** Privates  */

    private function innerNumeros(){
        //return 'INNER JOIN numeros_sorteados'
    }

    private function obj(array $dados){
        $obj = new StdClass();
        
        $obj->id     = $dados['id'];
        $obj->ano    = $dados['ano'];
        $obj->status = $dados['status'];
        $obj->numero = $dados['numero'];
        
        $obj->data   = new \DateTime($dados['data']);
        $obj->data   = $obj->data->format('d/m/Y');
        
        $diasdasemana = ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'];
        $obj->diasemana = date('w', strtotime($dados['data']));
        $obj->diasemana = $diasdasemana[$obj->diasemana];

        return $obj; 
    }


}