<?php
require 'autoload.php';

class Numeros{
    private $acesso;
    private $sorteio;
    static private $table = 'numeros_sorteados';
    
    public function __construct($sorteio = false){
        $this->acesso  = new Conexao();
        $this->sorteio = $sorteio; 
    }

    public function getNumeros($parts = false){
        $numeros = $this->acesso->select(self::$table, '*', false, [['sorteio_id', '=', $this->sorteio->id]], 'ORDER BY ordem ASC');
        if(is_array($numeros)){
            return (!$parts) ? $numeros : array_slice($numeros, 0, $parts);
        }else {
            return false;
        }
    } 

    private function obj(array $dados){
         
    }
}

