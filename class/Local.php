<?php
require 'autoload.php';

class Local{
    private $acesso;
    private $local_global;
    private $subpage;
    static private $table = 'locais';

    public $id;
    public $nome;
    public $link;

    public function __construct(){
        $this->acesso = new Conexao();

        $this->local_global  = filter_input(INPUT_GET, 'local');
        $this->subpage       = filter_input(INPUT_GET, 'page');
        
        $this->setLocalAtual();
    }

    public function subPages(){
        if($this->subpage !== ''){
            switch($this->subpage){
                case "sorteios":
                    require_once "sorteios.php";
                    break;
            }
            exit();
        }
    }
    
    private function setLocalAtual(){
        if($this->local_global == ''){
            require_once 'presenca.php';
            exit();
        }

        $local =$this->acesso->select(self::$table, '*', false,[['status', '=', 'A'], ['link', '=', $this->local_global]], ' LIMIT 1') [0];
        
        if(is_null($local)){
            require_once 'inexistente.php';
            exit();
        }

        $this->id   = $local['id'];
        $this->nome = $local['nome'];
        $this->link = $local['link'];
    }
}