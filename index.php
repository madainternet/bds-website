<?php require_once 'config.php'; ?>
<!DOCTYPE html>
<html lang="br">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="Premios, sorteios, sorte">
	<meta name="description" content="Brasil da Sorte">

	<title>Brasil da Sorte :: <?php echo $local->nome; ?></title>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/animate.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/component.css">

  <link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/vegas.min.css">
	<link rel="stylesheet" href="css/style.css">

	<!-- Google web font  -->
	<!-- <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,300' rel='stylesheet' type='text/css'> -->
  <link href="https://fonts.googleapis.com/css?family=Allerta|Archivo+Black|Baloo|Cabin|Cantora+One|Changa+One|Mitr|Rubik|Timmana" rel="stylesheet">
</head>
<body id="top" data-spy="scroll" data-offset="50" data-target=".navbar-collapse">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.0';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Preloader section -->

<div class="preloader">
     <div class="sk-spinner sk-spinner-pulse"></div>
</div>


<!-- Navigation section  -->

  <div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">

      <div class="navbar-header">
      <a href="http://www.brasildasorte.com.br" class="logomobile navbar-brand smoothScroll">
         <span style="color:#fcc500">BRASIL</span>DASORTE
      </a>
		 <a href="http://www.brasildasorte.com.br" class="navbar-brand smoothScroll">
      <img class="logo" width="70%" src="logo-Brasil-da-Sorte-sem-fundo-p.png"  alt="Logo Brasil da Sorte"/>
    </a>
		  
        <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon icon-bar"></span>
          <span class="icon icon-bar"></span>
          <span class="icon icon-bar"></span>
        </button>

      </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#top" class="smoothScroll"><span>Início</span></a></li>
            <li><a href="#about" class="smoothScroll"><span>Ganhadores</span></a></li>
            <li><a href="#sorteios" class="smoothScroll"><span>Sorteios</span></a></li>
			      <li><a href="regulamento.php" class="smoothScroll"><span>Regulamento</span></a></li>
            <li><a href="#contact" class="smoothScroll"><span>Contato</span></a></li>
          </ul>
       </div>

    </div>
  </div>


<!-- Home section -->

<section id="home">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">

      <div class="col-md-offset-1 col-md-10 col-sm-12 wow fadeInUp" data-wow-delay="0.3s">
        <br clear="all" />
        <br clear="all" />
        <hr />
        <!--
        <iframe src="https://player.jmvstream.com/dQwTcPpyVE4fjZF2cUFgNPk0SYtEvp" allowfullscreen frameborder="0" width="90%" height="400px">
        
        </iframe>
        -->
        <div class="fb-video" data-href="https://www.facebook.com/brasildasorte/videos/1952862534763887/" data-width="500" data-show-text="false"><blockquote cite="https://www.facebook.com/brasildasorte/videos/1952862534763887/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/brasildasorte/videos/1952862534763887/">16ª Extração do Brasil da Sorte (Tucurí)</a><p>16ª Extração Brasil da Sorte (Tucuruí)</p>Publicado por <a href="https://www.facebook.com/brasildasorte/">Brasil DA SORTE</a> em Quinta-feira, 14 de junho de 2018</blockquote></div>

		<p>Último Sorteio!
    <?php echo  $ult_sorteio->diasemana, ', ', 
                $ult_sorteio->data, 
                '. Extração ', 
                $ult_sorteio->ano,
                '/',
                $ult_sorteio->numero
                ?></p>
		    <h2 class="numeros">
        <?php 
        if($utl_sorteio_numeros->getNumeros() !== false){
          foreach($utl_sorteio_numeros->getNumeros(4) as $numero){
            echo $numero['numero'].' ';
          }
        }
        ?> 
        
        <a href='#' data-toggle="modal" data-target="#verTodosNumerosUltSorteio" title="ver todos os Números Sorteados">...</a></h2>
        <p class="wow fadeInUp" data-wow-delay="0.9s">
<!-- Acompanhei ao vivo o último sorteio do Brasil da Sorte ou veja <a rel="nofollow" href="#sorteios">outros sorteios</a> aqui.</p> -->
        <a href="#sorteios" class="smoothScroll btn btn-success btn-lg wow fadeInUp" data-wow-delay="0.2s">Todos os Sorteios </a>
      </div>
  
    </div>
  </div>
</section>

<!-- About section -->
	
<section id="about">
  <div class="container">
    <div class="row">

 

 <div class="col-md-9 col-sm-8 wow fadeInUp" data-wow-delay="0.2s">
        <div class="about-thumb">
          <h1>Últimos Resultados</h1>
          <p>Segunda 04/06/2018 - Leidiane Socorro Dias <br>
Números <a href=''>40 49 54 31 ...</a></p>
		  <p>Sábado 02/06/2018 - Paula Verdare <br>
Números <a href=''>59 40 60 55 ...</a></p>
        </div>
  </div>

  <div class="col-md-3 col-sm-4 wow fadeInUp about-img" data-wow-delay="0.6s">
    <img src="images/logotemp.jpeg" class="img-responsive" alt="About">
  </div>

    <div class="clearfix"></div>

      <div class="col-md-12 col-sm-12 wow fadeInUp" data-wow-delay="0.3s">
        <div class="section-title text-center">
          <h1>Veja os Ganhadores!  </h1>
          <h3> </h3>
        </div>
      </div>

      <!-- team carousel -->
	
      <div class="owl-carousel" id="team-carousel" style="font-style: italic">

      <div class="item col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="0.4s">
        <div class="team-thumb">
          <div class="image-holder">
            <img src="images/kauanne.jpg" class="img-responsive img-circle" alt="Kauanne">
          </div>
          <h2 class="heading">Kauanne Almeida </h2>
          <p class="description">Mora em Tucurui e ganhou 1500 reais</p>
        </div>
      </div>
	
      <div class="item col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="0.6s">
        <div class="team-thumb">
          <div class="image-holder">
            <img src="images/team-img2.jpg" class="img-responsive img-circle" alt="">
          </div>
          <h2 class="heading">Pedro Vitor dos Santos Monis<br>
e Luciente dos Santos Nunes</h2>
          <p class="description">Ganhadores do Prêmio de R$ 1.500,00</p>
        </div>
      </div>

      <div class="item col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="0.8s">
        <div class="team-thumb">
          <div class="image-holder">
            <img src="images/team-img3.jpg" class="img-responsive img-circle" alt="">
          </div>
          <h2 class="heading">...</h2>
          <p class="description">...</p>
        </div>
      </div>

      <div class="item col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="0.8s">
        <div class="team-thumb">
          <div class="image-holder">
            <img src="images/team-img4.jpg" class="img-responsive img-circle" alt="Sandy">
          </div>
          <h2 class="heading">...</h2>
          <p class="description">...</p>
        </div>
      </div>

      <div class="item col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="0.8s">
        <div class="team-thumb">
          <div class="image-holder">
            <img src="images/team-img5.jpg" class="img-responsive img-circle" alt="Lukia">
          </div>
          <h2 class="heading">...</h2>
          <p class="description">...</p>
        </div>
      </div>

      <div class="item col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="0.8s">
        <div class="team-thumb">
          <div class="image-holder">
            <img src="images/team-img6.jpg" class="img-responsive img-circle" alt="George">
          </div>
          <h2 class="heading">...</h2>
          <p class="description">...</p>
        </div>
      </div>

      <div class="item col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="0.8s">
        <div class="team-thumb">
          <div class="image-holder">
            <img src="images/team-img7.jpg" class="img-responsive img-circle" alt="Day">
          </div>
          <h2 class="heading">...</h2>
          <p class="description">...</p>
        </div>
      </div>

      <div class="item col-md-4 col-sm-6 wow fadeInUp" data-wow-delay="0.8s">
        <div class="team-thumb">
          <div class="image-holder">
            <img src="images/team-img8.jpg" class="img-responsive img-circle" alt="Lynn">
          </div>
          <h2 class="heading">...</h2>
          <p class="description">...</p>
        </div>
      </div>

      </div>

      <!-- end team carousel -->


    </div>
  </div>
</section>



<!-- Gallery section -->

<section id="sorteios">
  <div class="container">
    <div class="row">

      <div class="col-md-offset-2 col-md-8 col-sm-12 wow fadeInUp" data-wow-delay="0.3s">
        <div class="section-title">
          <h1>Transmissões</h1>
          <p>Veja em vídeo!</p>
        </div>
      </div>

      <ul class="grid cs-style-4">
        <li class="col-md-6 col-sm-6">
          <figure>
            <div>
              <video controls="" name="media" width="525" height="315">
                <source src="http://www.brasildasorte.com.br/video/tucurui-26-05-2018.mp4" type="video/mp4">
              </video>
            </div>			  
            
            <div></div>
            
            <figcaption>
              <h1>Prêmio 1500,00 Reais</h1>
              <small>Para a ganhadora Kauanne Almeida </small>
              <a href="#">Veja o vídeo</a>
            </figcaption>
         
          </figure>
        </li>

      </ul>

    </div>
  </div>
</section>


	
<!-- Contact section -->

<section id="contact">	 
	
  <div class="container">
	  
    <div class="row">

       <div class="col-md-offset-1 col-md-10 col-sm-12">

        <div class="col-lg-offset-1 col-lg-10 section-title wow fadeInUp" data-wow-delay="0.4s">
          <h1>Envie uma Mensagem</h1>
          <p>Preencha o formulário com suas dúvidas, sugestões ou reclamações e em breve retornaremos para você. </p>
        </div>

        <form action="#" method="post" class="wow fadeInUp" data-wow-delay="0.8s">
          <div class="col-md-6 col-sm-6">
            <input name="name" type="text" class="form-control" id="name" placeholder="Nome">
          </div>
          <div class="col-md-6 col-sm-6">
            <input name="email" type="email" class="form-control" id="email" placeholder="E-mail">
          </div>
          <div class="col-md-12 col-sm-12">
            <textarea name="message" rows="6" class="form-control" id="message" placeholder="Mensagem"></textarea>
          </div>
          <div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6">
            <input type="submit" class="form-control" value="ENVIAR">
          </div>
        </form>

      </div>

    </div>
  </div>
</section>


<!-- Footer section -->

<footer>
	<div class="container">
    
		<div class="row">

			<div class="col-md-12 col-sm-12">
            
                <ul class="social-icon"> 
                    <li><a href="#" class="fa fa-facebook wow fadeInUp" data-wow-delay="0.2s"></a></li>
                   <!--  <li><a href="#" class="fa fa-twitter wow fadeInUp" data-wow-delay="0.4s"></a></li> -->
                   <!--  <li><a href="#" class="fa fa-linkedin wow fadeInUp" data-wow-delay="0.6s"></a></li> -->
                    <li><a href="#" class="fa fa-instagram wow fadeInUp" data-wow-delay="0.8s"></a></li>
                   <!-- <li><a href="#" class="fa fa-google-plus wow fadeInUp" data-wow-delay="1.0s"></a></li> -->
                </ul>

				<p class="wow fadeInUp"  data-wow-delay="1s" >Copyright &copy; 2018 Brasil da Sorte | 
             <a href="http://www.madainternet.com.br" title="free css templates" target="_blank">MD Criativa</a></p>
                
			</div>
			
		</div>
        
	</div>
</footer>

<div class="modal fade" id="verTodosNumerosUltSorteio" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Último Sorteio</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <?php 
        if($utl_sorteio_numeros->getNumeros() !== false){
          echo '<h1>';
          foreach($utl_sorteio_numeros->getNumeros() as $numero){
            echo $numero['numero'].' - ';
          }
          echo '</h1>';
        }
      ?>
      </div>
    </div>
  </div>
</div>

<!-- Back top -->
<a href="#" class="go-top"><i class="fa fa-angle-up"></i></a>

<!-- Javascript  -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/vegas.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/toucheffects.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/custom.js"></script>


<script>
$(document).ready(function(){
  var screenw = $(window).width();
  if(screenw <= 650){
    $('.logo').hide();
    $('.logomobile').show();
  }else{
    $('.logo').show();
    $('.logomobile').hide();
  }
});
</script>

</body>
</html>