<?php
clearstatcache();
session_start();
spl_autoload_register(function ($class_name) {
    include 'class/'.$class_name . '.php';
});

$local    = new Local();
$local->subPages();

$sorteios = new Sorteio($local);

$ult_sorteio         = $sorteios->getUltimo();
$utl_sorteio_numeros = new Numeros($ult_sorteio);

