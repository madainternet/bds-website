<!DOCTYPE html>
<html lang="br">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="Premios, sorteios, sorte">
	<meta name="description" content="Brasil da Sorte">

	<title>Brasil da Sorte :: <?php echo $local->nome; ?></title>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/animate.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
  <link rel="stylesheet" href="css/component.css">

  <link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/vegas.min.css">
	<link rel="stylesheet" href="css/style.css">

	<!-- Google web font  -->
	<!-- <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,300' rel='stylesheet' type='text/css'> -->
  <link href="https://fonts.googleapis.com/css?family=Allerta|Archivo+Black|Baloo|Cabin|Cantora+One|Changa+One|Mitr|Rubik|Timmana" rel="stylesheet">
</head>
<body id="top" data-spy="scroll" data-offset="50" data-target=".navbar-collapse">


<!-- Preloader section -->

<div class="preloader">
     <div class="sk-spinner sk-spinner-pulse"></div>
</div>


<!-- Navigation section  -->

  <div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">

      <div class="navbar-header">
      <a href="http://www.brasildasorte.com.br" class="logomobile navbar-brand smoothScroll">
         <span style="color:#fcc500">BRASIL</span>DASORTE
      </a>
		 <a href="http://www.brasildasorte.com.br" class="navbar-brand smoothScroll">
      <img class="logo" width="70%" src="logo-Brasil-da-Sorte-sem-fundo-p.png"  alt="Logo Brasil da Sorte"/>
    </a>
		  
        <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon icon-bar"></span>
          <span class="icon icon-bar"></span>
          <span class="icon icon-bar"></span>
        </button>

      </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#top" class="smoothScroll"><span>Início</span></a></li>
            <li><a href="#about" class="smoothScroll"><span>Ganhadores</span></a></li>
            <li><a href="#sorteios" class="smoothScroll"><span>Sorteios</span></a></li>
			      <li><a href="regulamento.php" class="smoothScroll"><span>Regulamento</span></a></li>
            <li><a href="#contact" class="smoothScroll"><span>Contato</span></a></li>
          </ul>
       </div>

    </div>
  </div>


<!-- Home section -->

<section id="home">
  <div class="overlay"></div>
  <div class="container">
    
  </div>
</section>

<section id="contact">	 
	
  <div class="container">
	  
    <div class="row">

       <div class="col-md-offset-1 col-md-10 col-sm-12">

        <div class="col-lg-offset-1 col-lg-10 section-title wow fadeInUp" data-wow-delay="0.4s">
          <h1>Envie uma Mensagem</h1>
          <p>Preencha o formulário com suas dúvidas, sugestões ou reclamações e em breve retornaremos para você. </p>
        </div>

        <form action="#" method="post" class="wow fadeInUp" data-wow-delay="0.8s">
          <div class="col-md-6 col-sm-6">
            <input name="name" type="text" class="form-control" id="name" placeholder="Nome">
          </div>
          <div class="col-md-6 col-sm-6">
            <input name="email" type="email" class="form-control" id="email" placeholder="E-mail">
          </div>
          <div class="col-md-12 col-sm-12">
            <textarea name="message" rows="6" class="form-control" id="message" placeholder="Mensagem"></textarea>
          </div>
          <div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6">
            <input type="submit" class="form-control" value="ENVIAR">
          </div>
        </form>

      </div>

    </div>
  </div>
</section>


<!-- Footer section -->

<footer>
	<div class="container">
    
		<div class="row">

			<div class="col-md-12 col-sm-12">
            
                <ul class="social-icon"> 
                    <li><a href="#" class="fa fa-facebook wow fadeInUp" data-wow-delay="0.2s"></a></li>
                   <!--  <li><a href="#" class="fa fa-twitter wow fadeInUp" data-wow-delay="0.4s"></a></li> -->
                   <!--  <li><a href="#" class="fa fa-linkedin wow fadeInUp" data-wow-delay="0.6s"></a></li> -->
                    <li><a href="#" class="fa fa-instagram wow fadeInUp" data-wow-delay="0.8s"></a></li>
                   <!-- <li><a href="#" class="fa fa-google-plus wow fadeInUp" data-wow-delay="1.0s"></a></li> -->
                </ul>

				<p class="wow fadeInUp"  data-wow-delay="1s" >Copyright &copy; 2018 Brasil da Sorte | 
             <a href="http://www.madainternet.com.br" title="free css templates" target="_blank">MD Criativa</a></p>
                
			</div>
			
		</div>
        
	</div>
</footer>

<div class="modal fade" id="verTodosNumerosUltSorteio" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Último Sorteio</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>

<!-- Back top -->
<a href="#" class="go-top"><i class="fa fa-angle-up"></i></a>

<!-- Javascript  -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/vegas.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/toucheffects.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/custom.js"></script>


<script>
$(document).ready(function(){
  var screenw = $(window).width();
  if(screenw <= 650){
    $('.logo').hide();
    $('.logomobile').show();
  }else{
    $('.logo').show();
    $('.logomobile').hide();
  }
});
</script>

</body>
</html>