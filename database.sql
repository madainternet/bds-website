SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `brasildasorte`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ganhadores`
--

CREATE TABLE `ganhadores` (
  `id` int(11) NOT NULL,
  `sorteio_id` int(11) NOT NULL,
  `pessoa_id` int(11) NOT NULL,
  `valor` float(10,2) NOT NULL,
  `dt_cadastro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `locais`
--

CREATE TABLE `locais` (
  `id` int(11) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `link` varchar(60) NOT NULL,
  `status` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `locais`
--

INSERT INTO `locais` (`id`, `nome`, `link`, `status`) VALUES
(1, 'Tucuruí', 'tucurui', 'A'),
(2, 'Surubim', 'surubim', 'A');

-- --------------------------------------------------------

--
-- Estrutura da tabela `numeros_sorteados`
--

CREATE TABLE `numeros_sorteados` (
  `id` int(11) NOT NULL,
  `numero` int(2) NOT NULL,
  `sorteio_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoas`
--

CREATE TABLE `pessoas` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `apelido` varchar(60) DEFAULT NULL,
  `celular` varchar(15) DEFAULT NULL,
  `cep` varchar(8) DEFAULT NULL,
  `status` varchar(2) NOT NULL,
  `local_cadastro_id` int(11) NOT NULL,
  `dt_cadastro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sorteios`
--

CREATE TABLE `sorteios` (
  `id` int(11) NOT NULL,
  `ano` int(4) NOT NULL,
  `numero` int(11) NOT NULL,
  `status` varchar(2) NOT NULL,
  `local_id` int(11) NOT NULL,
  `dt_cadastro` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `transmissoes`
--

CREATE TABLE `transmissoes` (
  `id` int(11) NOT NULL,
  `chave` varchar(255) NOT NULL,
  `data` datetime NOT NULL,
  `status` varchar(1) NOT NULL,
  `sorteio_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ganhadores`
--
ALTER TABLE `ganhadores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ganhadores_pessoa_id` (`pessoa_id`),
  ADD KEY `fk_ganhadores_sorteio_id` (`sorteio_id`);

--
-- Indexes for table `locais`
--
ALTER TABLE `locais`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `numeros_sorteados`
--
ALTER TABLE `numeros_sorteados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_numeros_sorteados_sorteio_id` (`sorteio_id`);

--
-- Indexes for table `pessoas`
--
ALTER TABLE `pessoas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pessoa_local_id` (`local_cadastro_id`);

--
-- Indexes for table `sorteios`
--
ALTER TABLE `sorteios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sorteio_local_id` (`local_id`);

--
-- Indexes for table `transmissoes`
--
ALTER TABLE `transmissoes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_transmissao_sorteio_id` (`sorteio_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ganhadores`
--
ALTER TABLE `ganhadores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `locais`
--
ALTER TABLE `locais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `numeros_sorteados`
--
ALTER TABLE `numeros_sorteados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pessoas`
--
ALTER TABLE `pessoas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sorteios`
--
ALTER TABLE `sorteios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transmissoes`
--
ALTER TABLE `transmissoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `ganhadores`
--
ALTER TABLE `ganhadores`
  ADD CONSTRAINT `fk_ganhadores_pessoa_id` FOREIGN KEY (`pessoa_id`) REFERENCES `pessoas` (`id`),
  ADD CONSTRAINT `fk_ganhadores_sorteio_id` FOREIGN KEY (`sorteio_id`) REFERENCES `sorteios` (`id`);

--
-- Limitadores para a tabela `numeros_sorteados`
--
ALTER TABLE `numeros_sorteados`
  ADD CONSTRAINT `fk_numeros_sorteados_sorteio_id` FOREIGN KEY (`sorteio_id`) REFERENCES `sorteios` (`id`);

--
-- Limitadores para a tabela `pessoas`
--
ALTER TABLE `pessoas`
  ADD CONSTRAINT `fk_pessoa_local_id` FOREIGN KEY (`local_cadastro_id`) REFERENCES `locais` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `sorteios`
--
ALTER TABLE `sorteios`
  ADD CONSTRAINT `fk_sorteio_local_id` FOREIGN KEY (`local_id`) REFERENCES `locais` (`id`);

--
-- Limitadores para a tabela `transmissoes`
--
ALTER TABLE `transmissoes`
  ADD CONSTRAINT `fk_transmissao_sorteio_id` FOREIGN KEY (`sorteio_id`) REFERENCES `sorteios` (`id`);
COMMIT;
